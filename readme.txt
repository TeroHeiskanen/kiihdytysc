
===============================================================================
KIIHDYTYS
Autopeli
===============================================================================

1.Asennus
2.Tietoa ohjelmasta
3.K�ytt�ohjeet
4.Ohjelman tarvitsemat tiedostot
5.Ongelmat ja jatkokehitysideat
6.Toteutus
7.Tekij�t


===============================================================================
1. Asennus
===============================================================================

Ohjelma tulee .zip-tiedostossa ja erillisi� asennuksia ei paketin purkamisen
lis�ksi tarvita. Ohjelma testattu windows 7 -k�ytt�j�rjestelmill�.


===============================================================================
2. Tietoa ohjelmasta
===============================================================================

Toiminnalliset vaatimukset:
- Pelaajan pit�� pysty� ostamaan autoja (yksi tai useampia)
- Pelaajan pit�� pysty� myym��n autoja
- Pelaajan pit�� pysty� ostamaan/vaihtamaan autoon osia
- Pelaajan pit�� pysty� ajamaan autolla kisoja
- Pelaajan pit�� pysty� ottamaan lainaa pankista
- Pelaajan pit�� pysty� tallentamaan/lataamaan pelitilanne
- Pelaajan pit�� saada rahaa voitetuista kisoista

Toteuttamatta j��neet vaatimukset:
- Pelaajan pit�� pysty� vaikuttamaan kisan l�ht��n

Ei toiminnalliset vaatimukset:
- Peli-ikkunan koko pit�� pysy� vakiona
- Viritysosat ja autot ladataan XML-tiedostosta
- Pelin pit�� py�ri� jouhevasti
- Pelin pit�� py�ri� Windows 7 alustalla
- Pelin pit�� py�ri� ilman asennusta (copy/paste)

Yli alkuper�isten vaatimusten:
- Autoja ja osia voi lis�t�/poistaa/muokata auto- ja osamanagerissa
- Joka kisan j�lkeen osat menett�v�t suorituskyky��n
- Peliss� vaihtuu p�iv�t


===============================================================================
3. K�ytt�ohjeet
===============================================================================

Pelaajan pit�� alussa ostaa oma auto autokaupasta. Auto pit�� olla valittuna
pelaajan autotallista (vihre� tausta valitulla autolla), jotta sill� voi kisata.
Jos raha loppuu, pankista voi ottaa lainaa (huom. laina maksetaan korkoineen 
takaisin joka sunnuntai riippumatta siit�, milloin lainan on ottanut). 
Pelaajalla voi olla maksimissaan nelj� autoa omassa autotallissa. Osat hajoavat
hieman jokaisen kisan j�lkeen, mik� laskee niiden tehokkuuksia ja arvoa.

Itse kisassa ei tarvitse tehd� muuta kuin j�nnitt��. Voittopalkinto on 100�.

Ohjelman mukana tulee my�s manageri (huutomerkkinappi oikeassa alakulmassa),
jossa voi vaikka kesken pelin k�yd� lis��m�ss� ohjelmaan uusia autoja ja osia.
Jotta managerin muutokset n�kyisiv�t peliss�, saattaa peli tarvita uudelleen-
k�ynnistyksen.


===============================================================================
4. Ohjelman tarvitsemat tiedostot
===============================================================================

- Tarvittavat oliot tallennetaan XML-tiedostoihin luontiskriptill�
- Peli k�ytt�� png-kuvia, jotka tulevat zip-paketin mukana


===============================================================================
5. Ongelmat ja jatkokehitysideat
===============================================================================

Ongelmat/bugit:
- T�m�nhetkisess� versiossa ei pit�isi olla ohjelman suorituksen kannalta 
  kohtalokkaita bugeja
- Velka ei n�y, jos lataa pelin. Velka kuitenkin perit��n normaalisti
  sunnuntaina
- Rikkin�isell� eli nollakuntoisella autolla voi ajaa, mutta auto ei liiku.
  Ei v�ltt�m�tt� edes bugi
- Osan vaihtohinta ei v�ltt�m�tt� n�y aina oikein


Jatkokehitysideat:
- Toinen kisamuoto "Maili", miss� rata "scrollaa" autojen edetess�. K�yt�nn�ss� 
  pitempi p�tk� suoraa tiet�.
- Autojen ty�st�. Osien korjaus ja niiden yksil�llinen viritys
- Divisioonat kuskeille. Ei aina satunnaista vastustajaa
- Internetist� haetaan kuvia/tietoja osille/autoille


===============================================================================
6. Toteutus
===============================================================================

K�ytetty aika:
- Tero Heiskanen 64 tuntia
- Jaakko Sepp�nen 51 tuntia

Suurin osa koodaamisesta tapahtunut yhdess�, eli limitt�in olevia tunteja on 
useita kymmeni� (n. 40h).


Mit� opittu:
- XML kirjoitus ja luku
- Databinding eri tavoilla (suora bindaus, koodin kautta, multibinding)
- UI:n luonti ja sen elementtien hallitseminen (listboxit, gridit, napit, 
  kuvat jne.)
- Oliot (k�ytt�, bindaus, tallennus, lataus jne.)

Haasteet:
- Multibinding
- Yleinen tallennusskripti ohjelmassa k�ytett�ville olioille (template-metodi)
- Toimiva tapa saada useita eri n�kymi� p��llek�in
- Varttimailin toteutus WPF:ll�

Lis�tutkittavaa:
- Tietokantojen k�ytt�


===============================================================================
7. Tekij�t
===============================================================================

Tero Heiskanen G8337
Piste-ehdotus: 30/30

Jaakko Sepp�nen G7687
Piste-ehdotus: 30/30