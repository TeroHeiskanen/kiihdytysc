﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Windows.Controls;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Kiihdytys
{
    [Serializable]
    public class Car
    {
        #region PROPERTIES        

        private String name;
        private String icon;
        private int price;

        //Auton osat
        public Engine Engine { get; set; }
        public Exhaust Exhaust { get; set; }
        public Carburetor Carburetor { get; set; }
        public Tire Tires { get; set; }
        public Turbo Turbo { get; set; }
        public Cooler Cooler { get; set; }
        public Gearbox Gearbox { get; set; }

        [XmlElement("Name")]
        public String Name
        { 
            get 
            { 
                return name; 
            } 
            set 
            {
                name = value;
            } 
        }
        
        //Ikonin suhteellinen polku
        [XmlElement("Icon")]
        public String Icon 
        { 
            get
            {
                return icon;
            }
            set
            {
                icon = value;           
            } 
        }
        
        //Ikonin absoluuttinen polku
        public String IconFull
        {
            get
            {
                FileInfo info = new FileInfo(icon);
                return info.FullName;
            }
        }

        [XmlElement("Price")]
        public int Price
        {
            get
            {
                return price;
            }
            set
            {
                if (value > -1)
                {
                    price = value;
                }
            }
        }

        public int Speed
        {
            get
            {
                return Engine.SpeedUpgrade + Exhaust.SpeedUpgrade + Carburetor.SpeedUpgrade + Turbo.SpeedUpgrade + Cooler.SpeedUpgrade + Gearbox.SpeedUpgrade;
            }
        }

        public int Acceleration
        {
            get
            {
                return Engine.AccelerationUpgrade + Exhaust.AccelerationUpgrade + Carburetor.AccelerationUpgrade + Turbo.AccelerationUpgrade + Cooler.AccelerationUpgrade + Gearbox.AccelerationUpgrade;
            }
        }

        public int Friction
        {
            get
            {
                return Tires.FrictionUpgrade + Gearbox.FrictionUpgrade;
            }
        }
                
        //Nykyinen hinta auton osien kanssa arvonvähennyksellä
        public int CurrentPrice
        {
            get
            {
                return (price / 2) + Engine.CurrentPrice + Exhaust.CurrentPrice + Carburetor.CurrentPrice + Tires.CurrentPrice + Turbo.CurrentPrice + Cooler.CurrentPrice + Gearbox.CurrentPrice;
            }    
        }

        //Nykyinen hinta ilman arvonvähennystä
        public int OriginalPrice
        {
            get
            {
                return price + Engine.CurrentPrice + Exhaust.CurrentPrice + Carburetor.CurrentPrice + Tires.CurrentPrice + Turbo.CurrentPrice + Cooler.CurrentPrice + Gearbox.CurrentPrice;
            }
        }        
        
        #endregion

        #region CONSTRUCTORS
        private Car() 
        {
            name = "";
            icon = "";
            price = -1;
            Engine = new Engine();
            Exhaust = new Exhaust();
            Carburetor = new Carburetor();
            Tires = new Tire();
            Turbo = new Turbo();
            Cooler = new Cooler();
            Gearbox = new Gearbox();
        }

        public Car(String Name, String Icon, int Price, Engine engine, Exhaust exhaust, Carburetor carburetor, Tire tires, Turbo turbo, Cooler cooler, Gearbox gearbox)
        {
            name = Name;
            icon = Icon;
            price = Price;
            Engine = engine;
            Exhaust = exhaust;
            Carburetor = carburetor;
            Tires = tires;
            Turbo = turbo;
            Cooler = cooler;
            Gearbox = gearbox;
        }        

        #endregion

        #region OPERATOR_OVERLOAD

        //Operaattorien ylikuormitukset vertailulle
        public static bool operator ==(Car a, Car b)
        {
            if (System.Object.ReferenceEquals(a, b))
            {
                return true;
            }

            if (((Object)a == null) || ((Object)b == null))
            {
                return false;
            }

            return a.name == b.name;
        }

        public static bool operator !=(Car a, Car b)
        {
            return !(a == b);
        }

        public override String ToString()
        {
            return name + " " + price + "/" + OriginalPrice + "€";
        }
        #endregion

        #region METHOD

        //Tämä ajetaan joka kisan jälkeen, "hajottaa" osia
        public void DegradeParts()
        {
            Engine.ConditionDegrade();
            Exhaust.ConditionDegrade();
            Carburetor.ConditionDegrade();
            Tires.ConditionDegrade();
            Turbo.ConditionDegrade();
            Cooler.ConditionDegrade();
            Gearbox.ConditionDegrade();
        }

        #endregion
    }

    [XmlInclude(typeof(Engine))]
    [XmlInclude(typeof(Exhaust))]
    [XmlInclude(typeof(Carburetor))]
    [XmlInclude(typeof(Tire))]
    [XmlInclude(typeof(Turbo))]
    [XmlInclude(typeof(Cooler))]
    [XmlInclude(typeof(Gearbox))]
    [Serializable]
    public abstract class Part
    {
        protected String name;
        protected int price;
        protected int accelerationUpgrade;
        protected int speedUpgrade;
        protected int frictionUpgrade;
        protected int condition;
        protected Random rand;

        #region PROPERTIES

        [XmlElement("Name")]
        public String Name
        {
            get
            { 
                return name;
            } 
            set
            {
                name = value;
            } 
        }

        [XmlElement("Price")]
        public int Price
        { 
            get 
            {
                if (price < 0)
                {
                    return 0;
                }
                else
                {
                    return price;  
                }                           
            } 
            set
            {
                if (value > -1)
                {
                    price = value;
                }
            }
        }

        public int CurrentPrice
        {
            get
            {
                if (price < 0 || condition < 0)
                {
                    return 0;
                }
                else
                {
                    return (price * condition) / 100;
                }                
            }
        }

        //Osan antama kiihtyvyyden parannus
        [XmlElement("AccelerationUpgrade")]
        public int AccelerationUpgrade
        {
            get
            {
                if (accelerationUpgrade < 0)
                {
                    return 0;
                }
                else
                {
                    return (int)Math.Ceiling((float)accelerationUpgrade * (float)condition / 100); 
                }                
            } 
            set 
            {
                if (value > -1)
                {
                    accelerationUpgrade = value;
                }
            } 
        }

        //Osan antama nopeuden parannus
        [XmlElement("SpeedUpgrade")]
        public int SpeedUpgrade
        { 
            get 
            {
                if (speedUpgrade < 0)
                {
                    return 0;
                }
                else
                {
                    return (int)Math.Ceiling((float)speedUpgrade * (float)condition / 100); 
                }                
            } 
            set 
            {
                if (value > -1)
                {
                    speedUpgrade = value;
                }
            }
        }

        //Osan antama pidon parannus
        [XmlElement("FrictionUpgrade")]
        public int FrictionUpgrade
        {
            get
            {
                if (frictionUpgrade < 0)
                {
                    return 0;
                }
                else
                {
                    return (int)Math.Ceiling((float)frictionUpgrade * (float)condition / 100);
                }                
            } 
            set
            {
                if (value > -1)
                {
                    frictionUpgrade = value;
                }
            } 
        }

        //Osan kunto
        [XmlElement("Condition")]
        public int Condition 
        { 
            get 
            {
                if (condition < 0)
                {
                    return 0;
                }
                else
                {
                    return condition; 
                }                
            } 
            set {
                if (value > -1 && value < 101)
                {
                    condition = value;
                }
            } 
        }
        #endregion

        #region CONSTRUCTORS

        public Part() 
        {
            name = "";
            price = -1;
            accelerationUpgrade = -1;
            speedUpgrade = -1;
            frictionUpgrade = -1;
            condition = -1;
            rand = new Random();
            condition = -1;
        }

        public Part(String name, int price, int accelerationUpgrade, int speedUpgrade, int frictionUpgrade)
        {
            this.name = name;
            this.price = price;
            this.accelerationUpgrade = accelerationUpgrade;
            this.speedUpgrade = speedUpgrade;
            this.frictionUpgrade = frictionUpgrade;
            condition = 100;
            rand = new Random();
        }

        #endregion

        #region METHODS

        //Abstraktit metodit
        abstract public void ConditionDegrade();
        abstract public String DestructionMessage();              

        #endregion

        #region OVERRIDES

        public abstract override String ToString(); 

        //Ylikuormitukset vertailulle
        public static bool operator ==(Part a, Part b)
        {
            if (System.Object.ReferenceEquals(a, b))
            {
                return true;
            }

            if (((Object)a == null) || ((Object)b == null))
            {
                return false;
            }

            return a.name == b.name;
        }

        public static bool operator !=(Part a, Part b)
        {
            return !(a == b);
        }

        #endregion
    }
    #region PARTS


    public class Engine : Part
    {
        public Engine() { }
        public Engine(String Name, int Price, int AccelerationUpgrade, int SpeedUpgrade) : base(Name, Price, AccelerationUpgrade, SpeedUpgrade, 0) { }

        //Vähentää osan kuntoa
        public override void ConditionDegrade()
        {
            Condition -= rand.Next(4);
        }

        //Palauttaa "tuhoutumisviestin", ei käytetä missään
        public override String DestructionMessage()
        {
            if (rand.Next(0, 100) > 50)
            {
                return "Kampiakseli katkesi!";
            }
            else
            {
                return "Mäntä tuli kannesta läpi!";
            }
        }

        public override String ToString()
        {
            if (name == null || name == "")
            {
                return "Ei moottoria";
            }
            else
            {
                return name + " " + CurrentPrice + "€ N: " + speedUpgrade + " K: " + accelerationUpgrade;
            }            
        }
    }

    //Loput Osa-luokat vastaavat Engine luokkaa

    public class Exhaust : Part
    {
        public Exhaust() { }
        public Exhaust(String Name, int Price, int AccelerationUpgrade, int SpeedUpgrade) : base(Name, Price, AccelerationUpgrade, SpeedUpgrade, 0) { }

        public override void ConditionDegrade()
        {
            Condition -= rand.Next(2);
        }

        public override String DestructionMessage()
        {
            if (rand.Next(0, 100) > 50)
            {
                return "Pakoputki jäi matkalle!";
            }
            else
            {
                return "Pakoputki pamahti puhki!";
            }
        }

        public override String ToString()
        {
            if (name == null || name == "")
            {
                return "Ei pakoputkistoa";
            }
            else
            {
                return name + " " + CurrentPrice + "€ N: " + speedUpgrade + " K: " + accelerationUpgrade;
            }
        }
    }

    public class Carburetor : Part
    {
        public Carburetor() { }
        public Carburetor(String Name, int Price, int AccelerationUpgrade, int SpeedUpgrade) : base(Name, Price, AccelerationUpgrade, SpeedUpgrade, 0) { }

        public override void ConditionDegrade()
        {
            Condition -= rand.Next(2);
        }

        public override String DestructionMessage()
        {
            if (rand.Next(0, 100) > 50)
            {
                return "Kaasutin meni tukkoon!";
            }
            else
            {
                return "Kaasari tulvi löpöä!";
            }
        }

        public override String ToString()
        {
            if (name == null || name == "")
            {
                return "Ei kaasutinta";
            }
            else
            {
                return name + " " + CurrentPrice + "€ N: " + speedUpgrade + " K: " + accelerationUpgrade;
            }
        }
    }

    public class Tire : Part
    {
        public Tire() { }
        public Tire(String Name, int Price, int FrictionUpgrade) : base(Name, Price, 0, 0, FrictionUpgrade) { }

        public override void ConditionDegrade()
        {
            Condition -= rand.Next(1,5);
        }

        public override String DestructionMessage()
        {
            if (rand.Next(0, 100) > 50)
            {
                return "Ajoit naulaan ja kumi pamahti!";
            }
            else
            {
                return "Kumi pamahti!";
            }
        }

        public override String ToString()
        {
            if (name == null || name == "")
            {
                return "Ei renkaita";
            }
            else
            {
                return name + " " + CurrentPrice + "€ P: " + frictionUpgrade;
            }
        }
    }

    public class Turbo : Part
    {
        public Turbo()
        {
            name = "Ei turboa";
        }
        public Turbo(String Name, int Price, int AccelerationUpgrade, int SpeedUpgrade) : base(Name, Price, AccelerationUpgrade, SpeedUpgrade, 0) { }

        public override void ConditionDegrade()
        {
            Condition -= rand.Next(1,3);
        }

        public override String DestructionMessage()
        {
            if (rand.Next(0, 100) > 50)
            {
                return "Turbon laakeri leipoi kiinni!";
            }
            else
            {
                return "Turbo pamahti!";
            }
        }

        public override String ToString()
        {
            if (name == null || name == "")
            {
                return "Ei turboa";
            }
            else
            {
                return name + " " + CurrentPrice + "€ N: " + speedUpgrade + " K: " + accelerationUpgrade;
            }
        }
    }

    public class Cooler : Part
    {
        public Cooler()
        {
            name = "Ei intercooleria";
        }
        public Cooler(String Name, int Price, int AccelerationUpgrade, int SpeedUpgrade) : base(Name, Price, AccelerationUpgrade, SpeedUpgrade, 0) { }

        public override void ConditionDegrade()
        {
            Condition -= rand.Next(1);
        }

        public override String DestructionMessage()
        {
            if (rand.Next(0, 100) > 50)
            {
                return "Syylariin tuli reikä!";
            }
            else
            {
                return "Intercooler puhkesi!";
            }
        }

        public override String ToString()
        {
            if (name == null || name == "")
            {
                return "Ei intercooleria";
            }
            else
            {
                return name + " " + CurrentPrice + "€ N: " + speedUpgrade + " K: " + accelerationUpgrade;
            }
        }
    }

    public class Gearbox : Part
    {
        public Gearbox() { }
        public Gearbox(String Name, int Price, int AccelerationUpgrade, int SpeedUpgrade, int FrictionUpgrade) : base(Name, Price, AccelerationUpgrade, SpeedUpgrade, FrictionUpgrade) { }

        public override void ConditionDegrade()
        {
            Condition -= rand.Next(3);
        }

        public override String DestructionMessage()
        {
            if (rand.Next(0, 100) > 50)
            {
                return "Kytkin nosti savut ja hajosi!";
            }
            else
            {
                return "Vaihdekeppi katkesi!";
            }
        }

        public override String ToString()
        {
            if (name == null || name == "")
            {
                return "Ei vaihdelaatikkoa";
            }
            else
            {
                return name + " " + CurrentPrice + "€ N: " + speedUpgrade + " K: " + accelerationUpgrade + " P: " + frictionUpgrade;
            }
        }
    }

    #endregion
}
