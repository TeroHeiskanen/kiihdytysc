﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using System.Xml.Serialization;

namespace Kiihdytys
{
    //Pääikkuna
    public partial class MainWindow : Window
    {
        #region ENUM

        //ENUM:it eri näkymille
        enum Views
        {
            MainMenu_CreatePlayer, MainMenu_LoadPlayer, Game_QuarterMile, Game_FullMile,
            GameMenu_Info, GameMenu_CarLot, GameMenu_Garage, GameMenu_Parts,
            GameMenu_Machinery, GameMenu_Bank, GameMenu_Lottery
        }
        #endregion

        Game game;

        public MainWindow()
        {
            InitializeComponent();
        }

        #region METHODS

        //Näkymän vaihto
        private void changeView(Views view)
        {
            spGameMenu.Visibility = Visibility.Hidden;
            spMainMenu.Visibility = Visibility.Hidden;
            spGameMenu_Info.Visibility = Visibility.Hidden;
            spGameMenu_CarLot.Visibility = Visibility.Hidden;
            spGameMenu_Garage.Visibility = Visibility.Hidden;
            spGameMenu_Parts.Visibility = Visibility.Hidden;
            spGameMenu_Machinery.Visibility = Visibility.Hidden;
            spGameMenu_Bank.Visibility = Visibility.Hidden;
            spGameMenu_Lottery.Visibility = Visibility.Hidden;
            spMainMenu_CreatePlayer.Visibility = Visibility.Hidden;
            spMainMenu_LoadPlayer.Visibility = Visibility.Hidden;
            spDateAndMoney.Visibility = Visibility.Hidden;
            spGame_FullMile.Visibility = Visibility.Hidden;
            spGame_QuarterMile.Visibility = Visibility.Hidden;

            switch (view)
            {
                case Views.MainMenu_CreatePlayer:
                    spMainMenu.Visibility = Visibility.Visible;
                    spMainMenu_CreatePlayer.Visibility = Visibility.Visible;
                    break;
                case Views.MainMenu_LoadPlayer:
                    spMainMenu.Visibility = Visibility.Visible;
                    spMainMenu_LoadPlayer.Visibility = Visibility.Visible;
                    break;
                case Views.GameMenu_Info:
                    spGameMenu.Visibility = Visibility.Visible;
                    spGameMenu_Info.Visibility = Visibility.Visible;
                    spDateAndMoney.Visibility = Visibility.Visible;
                    break;
                case Views.GameMenu_CarLot:
                    spGameMenu.Visibility = Visibility.Visible;
                    spGameMenu_CarLot.Visibility = Visibility.Visible;
                    spDateAndMoney.Visibility = Visibility.Visible;
                    break;
                case Views.GameMenu_Garage:
                    spGameMenu.Visibility = Visibility.Visible;
                    spGameMenu_Garage.Visibility = Visibility.Visible;
                    spDateAndMoney.Visibility = Visibility.Visible;
                    break;
                case Views.GameMenu_Parts:
                    spGameMenu.Visibility = Visibility.Visible;
                    spGameMenu_Parts.Visibility = Visibility.Visible;
                    spDateAndMoney.Visibility = Visibility.Visible;
                    break;
                case Views.GameMenu_Machinery:
                    spGameMenu.Visibility = Visibility.Visible;
                    spGameMenu_Machinery.Visibility = Visibility.Visible;
                    spDateAndMoney.Visibility = Visibility.Visible;
                    break;
                case Views.GameMenu_Bank:
                    spGameMenu.Visibility = Visibility.Visible;
                    spGameMenu_Bank.Visibility = Visibility.Visible;
                    spDateAndMoney.Visibility = Visibility.Visible;
                    break;
                case Views.GameMenu_Lottery:
                    spGameMenu.Visibility = Visibility.Visible;
                    spGameMenu_Lottery.Visibility = Visibility.Visible;
                    spDateAndMoney.Visibility = Visibility.Visible;
                    break;
                case Views.Game_QuarterMile:
                    spGameMenu.Visibility = Visibility.Visible;
                    spGame_QuarterMile.Visibility = Visibility.Visible;
                    spDateAndMoney.Visibility = Visibility.Visible;
                    break;
                case Views.Game_FullMile:
                    spGameMenu.Visibility = Visibility.Visible;
                    spGame_FullMile.Visibility = Visibility.Visible;
                    spDateAndMoney.Visibility = Visibility.Visible;
                    break;
            }
        }

        //Hakee tallennetut pelit
        private void loadSaveGames()
        {
            try
            {
                if (Directory.Exists(Variables.SAVE_DIR))
                {
                    String[] dirFiles = Directory.GetFiles(Variables.SAVE_DIR);
                    List<FileInfo> saveFiles = new List<FileInfo>();
                    List<Game> saveGames = new List<Game>();

                    foreach (var item in dirFiles)
                    {
                        saveFiles.Add(new FileInfo(item));
                    }

                    foreach (var item in saveFiles)
                    {
                        if (item.Extension == ".sav")
                        {                        
                            saveGames.Add(XmlSerialize<Game>.Deserialize(item.ToString()));                       
                        }
                    }

                    lbPlayers.ItemsSource = saveGames;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        //Päivittää autokauppa-näkymän
        private void updateCarLotView()
        {
            if (cbCarLot.IsLoaded && game != null)
            {
                imgCarShopPlayerCar.Source = null;
                imgCarShopCar.Source = null;

                if (game.CurrentPlayer.CurrentCar != null)
                {
                    txtCarLotCurrentCarName.Text = game.CurrentPlayer.CurrentCar.Name;
                    txtCarLotCurrentCarPrice.Text = game.CurrentPlayer.CurrentCar.CurrentPrice.ToString();
                    btnSellSelectedCar.IsEnabled = true;
                    setImage(ref imgCarShopPlayerCar, game.CurrentPlayer.CurrentCar.IconFull);
                }
                else
                {
                    txtCarLotCurrentCarName.Text = "";
                    txtCarLotCurrentCarPrice.Text = "";
                    btnSellSelectedCar.IsEnabled = false;
                }

                if (cbCarLot.HasItems)
                {
                    btnBuyCar.IsEnabled = true;
                }
                else
                {
                    btnBuyCar.IsEnabled = false;
                }

                Car temp = (Car)cbCarLot.SelectedItem;
                lbCarLotParts.Items.Clear();
                lbCarLotParts.Items.Add(temp.Engine);
                lbCarLotParts.Items.Add(temp.Exhaust);
                lbCarLotParts.Items.Add(temp.Carburetor);
                lbCarLotParts.Items.Add(temp.Tires);
                lbCarLotParts.Items.Add(temp.Gearbox);
                lbCarLotParts.Items.Add(temp.Turbo);
                lbCarLotParts.Items.Add(temp.Cooler);

                txtCarLotAcceleration.Text = temp.Acceleration.ToString();
                txtCarLotFriction.Text = temp.Friction.ToString();
                txtCarLotSpeed.Text = temp.Speed.ToString();
                txtCarLotPrice.Text = temp.OriginalPrice.ToString() + "€";
                
                setImage(ref imgCarShopCar, temp.IconFull);                
            }
        }

        //Päivittää autotalli-näkymän
        private void updateGarageTreeView()
        {
            twPlayerCars.Items.Clear();
            txtGarageStats.Text = "";
            foreach (Car item in game.CurrentPlayer.Cars)
            {
                TreeViewItem twItem = new TreeViewItem();
                twItem.Header = item;

                twItem.Items.Add(item.Engine);
                twItem.Items.Add(item.Exhaust);
                twItem.Items.Add(item.Carburetor);
                twItem.Items.Add(item.Tires);
                twItem.Items.Add(item.Turbo);
                twItem.Items.Add(item.Cooler);
                twItem.Items.Add(item.Gearbox);

                twPlayerCars.Items.Add(twItem);
            }

            if (game.CurrentPlayer.CurrentCar != null)
            {
                ((TreeViewItem)twPlayerCars.Items[game.CurrentPlayer.CarIndex]).Background = Brushes.Lime;

                txtGarageStats.Text = "Valittu auto:\n" + game.CurrentPlayer.CurrentCar.Name + 
                                  "\nN: " + game.CurrentPlayer.CurrentCar.Speed +
                                  " K: " + game.CurrentPlayer.CurrentCar.Acceleration +
                                  " P: " + game.CurrentPlayer.CurrentCar.Friction;
                setImage(ref imgGarageCurrentCar, game.CurrentPlayer.CurrentCar.IconFull);
            }

            
        }

        //Päivittää osa-näkymän
        private void updatePartShopView()
        {
            if (cbPartShopCategory.IsLoaded)
            {
                cbPartShopPart.IsEnabled = false;
                cbPartShopCategory.IsEnabled = false;
                txtPartShopPlayerPart.Text = "";
                btnChangePart.IsEnabled = false;

                if (game != null && game.CurrentPlayer.CurrentCar != null)
                {
                    cbPartShopPart.IsEnabled = true;
                    cbPartShopCategory.IsEnabled = true;

                    if (cbPartShopCategory.SelectedItem != null)
                    {
                        int partPrice = 0;
                        Car car = game.CurrentPlayer.CurrentCar;

                        switch (((ComboBoxItem)cbPartShopCategory.SelectedItem).Content.ToString())
                        {
                            case "Moottorit":
                                cbPartShopPart.ItemsSource = Variables.Engines;
                                txtPartShopPlayerPart.Text = car.Engine.ToString();
                                partPrice = car.Engine.CurrentPrice;
                                break;
                            case "Pakoputkistot":
                                cbPartShopPart.ItemsSource = Variables.Exhausts;
                                txtPartShopPlayerPart.Text = car.Exhaust.ToString();
                                partPrice = car.Exhaust.CurrentPrice;
                                break;
                            case "Kaasuttimet":
                                cbPartShopPart.ItemsSource = Variables.Carburetors;
                                txtPartShopPlayerPart.Text = car.Carburetor.ToString();
                                partPrice = car.Carburetor.CurrentPrice;
                                break;
                            case "Renkaat":
                                cbPartShopPart.ItemsSource = Variables.Tires;
                                txtPartShopPlayerPart.Text = car.Tires.ToString();
                                partPrice = car.Tires.CurrentPrice;
                                break;
                            case "Turbot":
                                cbPartShopPart.ItemsSource = Variables.Turbos;
                                txtPartShopPlayerPart.Text = car.Turbo.ToString();
                                partPrice = car.Turbo.CurrentPrice;
                                break;
                            case "Intercoolerit":
                                cbPartShopPart.ItemsSource = Variables.Coolers;
                                txtPartShopPlayerPart.Text = car.Cooler.ToString();
                                partPrice = car.Cooler.CurrentPrice;
                                break;
                            case "Vaihdelaatikot":
                                cbPartShopPart.ItemsSource = Variables.Gearboxes;
                                txtPartShopPlayerPart.Text = car.Gearbox.ToString();
                                partPrice = car.Gearbox.CurrentPrice;
                                break;
                            default:
                                break;
                        }
                        if (cbPartShopPart.Items.Count > 0 && cbPartShopPart.SelectedItem == null)
                        {
                            cbPartShopPart.SelectedIndex = 0;
                        }

                        txtPartShopSwapPrice.Text = "Vaihtohinta: " + (((Part)cbPartShopPart.SelectedItem).CurrentPrice - partPrice) + "€";
                        btnChangePart.IsEnabled = true;
                    }                    
                }
            }
        }

        //Lopettaa pelin ja heittää takaisin päävalikkoon
        private void quitGame()
        {
            game = null;
            changeView(Views.MainMenu_CreatePlayer);
            grdGame.DataContext = null;
        }

        //Tarkastaa onko pelaajalla riittävästi rahaa
        private bool checkIfEnoughMoney(int money)
        {
            if (game.CurrentPlayer.Money > money)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        //Vaihtaa Image-elementin kuvan annetun polun perusteella
        private void setImage(ref Image img, String path)
        {
            Uri uri = new Uri(path);
            try
            {
                img.Source = new BitmapImage(uri);
            }
            catch (Exception)
            {               
            }            
        }

        //Vaihtaa autojen kuvien sijaintia varttimailikisassa
        private void moveCars(int cpuOffset, int playerOffset)
        {
            imgCpuCar.Margin = new Thickness(40, 10 + cpuOffset, 0, 0);            
            imgPlayerCar.Margin = new Thickness(0, 10 + playerOffset, 135, 0);
        }

        //Sisältää logiikan varttimailikisaan
        private void quarterRace()
        {
            if (game.CurrentPlayer.CurrentCar != null)
            {
                //Vaihtaa näkymän
                changeView(Views.Game_QuarterMile);
                spGameMenu.Visibility = Visibility.Hidden;

                //Hakee autot
                Random rand = new Random();
                Car cpuCar = Variables.Cars[rand.Next(Variables.Cars.Count)];
                Car playerCar = game.CurrentPlayer.CurrentCar;

                //Hakee kuvat liikennevaloille
                FileInfo noLight = new FileInfo("data\\icons\\nolight.png");
                FileInfo greenLight = new FileInfo("data\\icons\\greenlight.png");
                FileInfo yellowLight = new FileInfo("data\\icons\\yellowlight.png");
                FileInfo redLight = new FileInfo("data\\icons\\redlight.png");

                setImage(ref imgCpuCar, cpuCar.IconFull);
                setImage(ref imgPlayerCar, playerCar.IconFull);

                //Alustaa sijainnit
                int cpuOffset = 0, playerOffset = 0;
                int cpuSpeed = 0, playerSpeed = 0;
                moveCars(0, 0);                

                //Aloittaa lähtölaskennan
                setImage(ref imgFirstLight, redLight.FullName);
                setImage(ref imgSecondLight, noLight.FullName);
                setImage(ref imgThirdLight, noLight.FullName);

                //Päivittää UI:n valonvaihtojen välillä
                //Yksi refresh ja pitkä Thread.Sleep() ei toiminut,
                //joten loopataan refreshiä ja lyhyttä Thread.Sleepiä
                for (int i = 0; i < 12; i++)
                {
                    Refresh(Application.Current.MainWindow);
                    Thread.Sleep(50);
                }                

                setImage(ref imgSecondLight, yellowLight.FullName);

                Refresh(Application.Current.MainWindow);
                for (int i = 0; i < 12; i++)
                {
                    Refresh(Application.Current.MainWindow);
                    Thread.Sleep(50);
                }

                setImage(ref imgFirstLight, noLight.FullName);
                setImage(ref imgSecondLight, noLight.FullName);
                setImage(ref imgThirdLight, greenLight.FullName);

                Refresh(Application.Current.MainWindow);

                //Loopataan kunnes jompikumpi auto on maalissa
                while ((cpuOffset + (int)imgCpuCar.ActualHeight) < 390 && (playerOffset + (int)imgPlayerCar.ActualHeight) < 390)
                {
                    //Lasketaan autojen hetkelliset nopeudet
                    cpuSpeed = (calculateCurrentSpeed(cpuCar, cpuSpeed));
                    playerSpeed = (calculateCurrentSpeed(playerCar, playerSpeed));

                    //Siirretään autoja nopeuden mukaan
                    cpuOffset += cpuSpeed / 10;
                    playerOffset += playerSpeed / 10;

                    moveCars(cpuOffset, playerOffset);

                    //Päivitetään UI, "nukutaan" 33ms
                    //Saadaan n. 30FPS
                    Refresh(Application.Current.MainWindow);

                    Thread.Sleep(33);
                }

                //Vaihdetaan näkymä takaisin
                spGameMenu.Visibility = Visibility.Visible;

                //Tarkastetaan kumpi voitti ja annetaan sen mukaan ilmoitus ja rahaa
                if (imgPlayerCar.Margin.Top > imgCpuCar.Margin.Top)
                {
                    MessageBox.Show("Voitit!");
                    game.CurrentPlayer.Money += 100;
                }
                else if (imgPlayerCar.Margin.Top == imgCpuCar.Margin.Top)
                {
                    MessageBox.Show("Tasapeli!");
                }
                else
                {
                    MessageBox.Show("Hävisit!");
                }

                //Kulutetaan auton osia
                game.CurrentPlayer.CurrentCar.DegradeParts();
            }
            else
            {
                MessageBox.Show("Valitse ensin auto!");
            }
        }

        //Laskee hetkellisen nopeuden
        private int calculateCurrentSpeed(Car car, int speed)
        {
            //Tarkastaa onko auto saavuttanut jo huippunopeuden
            if (speed != car.Speed)
            {
                //Hakee hetkellisen kiihtyvyyden
                int acceleration = calculateCurrentAcceleration(car, speed);
                
                //Nostaa nopeutta hetkellisen kiihtyvyyden perusteella
                int newSpeed = speed + acceleration;

                //Tarkastaa meneekö uusi nopeus huippunopeuden yli ja palauttaa
                //oikean nopeuden
                if (newSpeed > car.Speed)
                {
                    return car.Speed;
                }
                else
                {
                    return newSpeed;
                }
            }
            else
            {
                return car.Speed;
            }
        }

        //Laskee hetkellisen kiihtyvyyden
        private int calculateCurrentAcceleration(Car car, int speed)
        {
            //Laskee kiihtyvyyden nopeuden perusteella (ottaa siis huomioon "ilmanvastuksen")
            float acceleration = (float)car.Acceleration * (1 - ((float)speed / (float)car.Speed));
            
            //Jos kiihtyvyys on yli auton pidon, palautetaan pidon rajoittama nopeus
            if ((int)acceleration > car.Friction)
            {
                return car.Friction;
            }
            else
            {
                return (int)acceleration;
            }            
        }

        //UI:n päivitystä kesken kisan
        private delegate void NoArgDelegate();

        public static void Refresh(DependencyObject obj)
        {
            obj.Dispatcher.Invoke(System.Windows.Threading.DispatcherPriority.ApplicationIdle,

            (NoArgDelegate)delegate { });
        }

        #endregion
        
        //Sisältää kaikkien nappien tapahtumankäsittelijät
        #region BUTTOSET
        private void btnExit_Click(Object sender, RoutedEventArgs e)
        {
            Application.Current.Shutdown();
        }

        private void btnStart_Click(Object sender, RoutedEventArgs e)
        {            
            try
            {
                Variables.LoadComponents();
                cbCarLot.ItemsSource = Variables.Cars;

                changeView(Views.GameMenu_Info);

                game = new Game(txtPlayer.Text, cbDifficulty.SelectedIndex + 1);

                grdGame.DataContext = game;
                game.NextDay();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnExitAndSave_Click(Object sender, RoutedEventArgs e)
        {
            game.SaveGame();
            quitGame();
        }

        private void btnStartNew_Click(Object sender, RoutedEventArgs e)
        {
            changeView(Views.MainMenu_CreatePlayer);
        }

        private void btnLoadSaveGame_Click(Object sender, RoutedEventArgs e)
        {
            changeView(Views.MainMenu_LoadPlayer);
            loadSaveGames();
        }

        private void btnQuarterMile_Click(Object sender, RoutedEventArgs e)
        {   
            quarterRace();
            
            game.NextDay();

            if (game.CurrentPlayer.Money < 0)
            {
                MessageBox.Show(Application.Current.MainWindow, "Teit konkurssin!");
                quitGame();
            }
            else if (game.CurrentPlayer.Debt == 0)
            {
                txtDebt.Visibility = Visibility.Hidden;
                txtIsDebt.Visibility = Visibility.Hidden;
            }
        }

        private void btnFullMile_Click(Object sender, RoutedEventArgs e)
        {
            changeView(Views.Game_FullMile);
            game.NextDay();
            game.CurrentPlayer.Money += 100;
            if (game.CurrentPlayer.Money < 0)
            {
                MessageBox.Show(Application.Current.MainWindow, "Teit konkurssin!");
                quitGame();
            }
            else if (game.CurrentPlayer.Debt == 0)
            {
                txtDebt.Visibility = Visibility.Hidden;
                txtIsDebt.Visibility = Visibility.Hidden;
            }
        }

        private void BtnCarLot_Click(object sender, RoutedEventArgs e)
        {
            cbCarLot.SelectedIndex = 0;
            updateCarLotView();
            changeView(Views.GameMenu_CarLot);
        }

        private void BtnGarage_Click(object sender, RoutedEventArgs e)
        {
            updateGarageTreeView();
            changeView(Views.GameMenu_Garage);
        }

        private void BtnParts_Click(object sender, RoutedEventArgs e)
        {
            updatePartShopView();
            changeView(Views.GameMenu_Parts);
        }

        private void BtnMachinery_Click(object sender, RoutedEventArgs e)
        {
            changeView(Views.GameMenu_Machinery);
        }

        private void BtnBank_Click(object sender, RoutedEventArgs e)
        {
            changeView(Views.GameMenu_Bank);
        }

        private void BtnLottery_Click(object sender, RoutedEventArgs e)
        {
            changeView(Views.GameMenu_Lottery);
        }

        private void btnLoadGame_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Variables.LoadComponents();
                cbCarLot.ItemsSource = Variables.Cars;      
                game = (Game)lbPlayers.SelectedItem;
                grdGame.DataContext = game;
                changeView(Views.GameMenu_Info);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }            
        }

        private void btnDeleteSavegame_Click(object sender, RoutedEventArgs e)
        {
            FileInfo file = new FileInfo(Variables.SAVE_DIR + ((Game)lbPlayers.SelectedItem).CurrentPlayer.Name + ".sav");
            file.Delete();
            loadSaveGames();
        }

        private void btnLaunchManager_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Variables.LoadComponents();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            Manager manager = new Manager();
            manager.Show();
        }

        private void btnBuyCar_Click(object sender, RoutedEventArgs e)
        {
            int price = ((Car)cbCarLot.SelectedItem).OriginalPrice;

            if (game.CurrentPlayer.RoomForCars)
            {
                if (price <= game.CurrentPlayer.Money)
                {
                    game.CurrentPlayer.Money -= price;
                    game.CurrentPlayer.AddCar((Car)cbCarLot.SelectedItem);
                    MessageBox.Show("Ostettu!");

                    updateCarLotView();
                }
                else
                {
                    MessageBox.Show("Sinulla ei ole tarpeeksi rahaa!");
                }
            }
            else
            {
                MessageBox.Show("Sinulla ei ole tilaa autoille!");
            }
        }

        private void btnChooseCar_Click(object sender, RoutedEventArgs e)
        {
            if (twPlayerCars.SelectedItem is TreeViewItem && ((TreeViewItem)twPlayerCars.SelectedItem).Header is Car)
            {
                for (int i = 0; i < twPlayerCars.Items.Count; i++)
                {
                    if (((TreeViewItem)twPlayerCars.Items[i]).IsSelected)
                    {
                        game.CurrentPlayer.CarIndex = i;
                    }
                }

                updateGarageTreeView();
            }
        }

        private void btnSellSelectedCar_Click(object sender, RoutedEventArgs e)
        {
            int index = game.CurrentPlayer.CarIndex;
            if (index > -1 && index < game.CurrentPlayer.Cars.Count)
            {
                int price = game.CurrentPlayer.CurrentCar.CurrentPrice;
                try
                {
                    game.CurrentPlayer.Cars.RemoveAt(index);
                    game.CurrentPlayer.Money += price;
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
            updateCarLotView();
        }

        private void btn_Loan500_Click(object sender, RoutedEventArgs e)
        {
            txtIsDebt.Visibility = Visibility.Visible;
            txtDebt.Visibility = Visibility.Visible;
            game.LoanMoney(500);
        }

        private void btn_Loan1000_Click(object sender, RoutedEventArgs e)
        {
            txtIsDebt.Visibility = Visibility.Visible;
            txtDebt.Visibility = Visibility.Visible;
            game.LoanMoney(1000);
        }

        private void btn_Loan2000_Click(object sender, RoutedEventArgs e)
        {
            txtIsDebt.Visibility = Visibility.Visible;
            txtDebt.Visibility = Visibility.Visible;
            game.LoanMoney(2000);
        }

        private void btn_Loan4000_Click(object sender, RoutedEventArgs e)
        {
            txtIsDebt.Visibility = Visibility.Visible;
            txtDebt.Visibility = Visibility.Visible;
            game.LoanMoney(4000);
        }

        private void btn_Loan6000_Click(object sender, RoutedEventArgs e)
        {
            txtIsDebt.Visibility = Visibility.Visible;
            txtDebt.Visibility = Visibility.Visible;
            game.LoanMoney(6000);
        }

        private void btn_Loan10000_Click(object sender, RoutedEventArgs e)
        {
            txtIsDebt.Visibility = Visibility.Visible;
            txtDebt.Visibility = Visibility.Visible;
            game.LoanMoney(10000);
        }

        private void btnChangePart_Click(object sender, RoutedEventArgs e)
        {
            int exchangePrice = 0;
            Car car = game.CurrentPlayer.CurrentCar;
            Part part = (Part)cbPartShopPart.SelectedItem;

            exchangePrice -= part.Price;

            switch (((ComboBoxItem)cbPartShopCategory.SelectedItem).Content.ToString())
            {
                case "Moottorit":
                    exchangePrice += car.Engine.CurrentPrice;
                    checkIfEnoughMoney(-exchangePrice);
                    game.CurrentPlayer.CurrentCar.Engine = (Engine)part;
                    break;
                case "Pakoputkistot":
                    exchangePrice += car.Exhaust.CurrentPrice;
                    checkIfEnoughMoney(-exchangePrice);
                    game.CurrentPlayer.CurrentCar.Exhaust = (Exhaust)part;
                    break;
                case "Kaasuttimet":
                    exchangePrice += car.Carburetor.CurrentPrice;
                    checkIfEnoughMoney(-exchangePrice);
                    game.CurrentPlayer.CurrentCar.Carburetor = (Carburetor)part;
                    break;
                case "Renkaat":
                    exchangePrice += car.Tires.CurrentPrice;
                    checkIfEnoughMoney(-exchangePrice);
                    game.CurrentPlayer.CurrentCar.Tires = (Tire)part;
                    break;
                case "Turbot":
                    exchangePrice += car.Turbo.CurrentPrice;
                    checkIfEnoughMoney(-exchangePrice);
                    game.CurrentPlayer.CurrentCar.Turbo = (Turbo)part;
                    break;
                case "Intercoolerit":
                    exchangePrice += car.Cooler.CurrentPrice;
                    checkIfEnoughMoney(-exchangePrice);
                    game.CurrentPlayer.CurrentCar.Cooler = (Cooler)part;
                    break;
                case "Vaihdelaatikot":
                    exchangePrice += car.Gearbox.CurrentPrice;
                    checkIfEnoughMoney(-exchangePrice);
                    game.CurrentPlayer.CurrentCar.Gearbox = (Gearbox)part;
                    break;
                default:
                    break;
            }

            game.CurrentPlayer.Money += exchangePrice;

            updatePartShopView();

            MessageBox.Show("Osa vaihdettu!");
        }

        #endregion

        //Sisältää muut tapahtumankäsittelyt
        #region ACTIONHANDLING

        private void lbPlayers_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (lbPlayers.Items.Count > 0)
            {
                btnDeleteSavegame.IsEnabled = true;
                btnLoadGame.IsEnabled = true;
            }
            else
            {
                btnDeleteSavegame.IsEnabled = false;
                btnLoadGame.IsEnabled = false;
            }
        }

        private void cbCarLot_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {            
            updateCarLotView();
        }   

        private void cbPartShopCategory_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            updatePartShopView();
        }

        private void cbPartShopPart_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            updatePartShopView();
        }

        #endregion
    }
}
