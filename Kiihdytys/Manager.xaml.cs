﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Kiihdytys
{
    //Manager-ikkuna
    public partial class Manager : Window
    {
        public Manager()
        {
            InitializeComponent();

            //Lähteet combobokseille
            cbEngines.ItemsSource = Variables.Engines;
            cbExhausts.ItemsSource = Variables.Exhausts;            
            cbCarburetors.ItemsSource = Variables.Carburetors;
            cbTires.ItemsSource = Variables.Tires;
            cbTurbos.ItemsSource = Variables.Turbos;
            cbCoolers.ItemsSource = Variables.Coolers;
            cbGearboxes.ItemsSource = Variables.Gearboxes;
        }

        #region PART

        //Tarkastaa osien lisäyksen eri kentät
        //Heittää exceptionin jos jotain puuttuu
        private void checkPartFields()
        {
            String name = txtPartName.Text;
            if (name == null || name.Length == 0)
            {
                throw new Exception("Nimi ei voi olla tyhjä!");
            }

            int acceleration = 0;
            if (txtPartAcceleration.IsEnabled)
            {
                try
                {
                    acceleration = Int32.Parse(txtPartAcceleration.Text);
                    if (acceleration < 0)
                    {
                        throw new Exception("Kiihtyvyys ei voi olla negatiivinen!");
                    }
                }
                catch (Exception)
                {
                    throw new Exception("Virheellinen kiihtyvyyden arvo!");
                }
            }

            int speed = 0;
            if (txtPartSpeed.IsEnabled)
            {
                try
                {
                    speed = Int32.Parse(txtPartSpeed.Text);
                    if (speed < 0)
                    {
                        throw new Exception("Nopeus ei voi olla negatiivinen!");
                    }
                }
                catch (Exception)
                {
                    throw new Exception("Virheellinen nopeuden arvo!");
                }
            }

            int friction = 0;
            if (txtPartFriction.IsEnabled)
            {
                try
                {
                    friction = Int32.Parse(txtPartFriction.Text);
                    if (friction < 0)
                    {
                        throw new Exception("Pito ei voi olla negatiivinen!");
                    }
                }
                catch (Exception)
                {
                    throw new Exception("Virheellinen pidon arvo!");
                }
            }

            int price = 0;
            try
            {
                price = Int32.Parse(txtPartPrice.Text);
                if (price < 0)
                {
                    throw new Exception("Hinta ei voi olla negatiivinen");
                }
            }
            catch (Exception)
            {
                throw new Exception("Virheellinen hinnan arvo");
            }
        }

        //Lisää osan kokoelmiin
        private void addPart()
        {
            try
            {
                checkPartFields();

                String name = txtPartName.Text;
                int price = Int32.Parse(txtPartPrice.Text);

                int speed = 0;
                int acceleration = 0;
                int friction = 0;

                if (txtPartSpeed.IsEnabled)
                {
                    speed = Int32.Parse(txtPartSpeed.Text);
                }
                if (txtPartAcceleration.IsEnabled)
                {
                    acceleration = Int32.Parse(txtPartAcceleration.Text);
                }
                if (txtPartFriction.IsEnabled)
                {
                    friction = Int32.Parse(txtPartFriction.Text);
                }

                switch (((ComboBoxItem)cbPartTypes.SelectedItem).Name)
                {
                    case "cbiEngine":
                        Variables.Engines.Add(new Engine(name, price, acceleration, speed));
                        break;
                    case "cbiExhaust":
                        Variables.Exhausts.Add(new Exhaust(name, price, acceleration, speed));
                        break;
                    case "cbiCarburetor":
                        Variables.Carburetors.Add(new Carburetor(name, price, acceleration, speed));
                        break;
                    case "cbiTires":
                        Variables.Tires.Add(new Tire(name, price, friction));
                        break;
                    case "cbiTurbo":
                        Variables.Turbos.Add(new Turbo(name, price, acceleration, speed));
                        break;
                    case "cbiCooler":
                        Variables.Coolers.Add(new Cooler(name, price, acceleration, speed));
                        break;
                    case "cbiGearbox":
                        Variables.Gearboxes.Add(new Gearbox(name, price, acceleration, speed, friction));
                        break;
                    default:
                        break;
                }
                Variables.SaveComponents();
            }
            catch (Exception ex)
            {
                txtStatusBar.Text = ex.Message;
            }           
        }

        #region ACTIONHANDLING
        private void btnAddPart_Click(object sender, RoutedEventArgs e)
        {
            addPart();           
        }

        private void cbPartTypes_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            lbParts.ItemsSource = null;

            txtPartAcceleration.IsEnabled = false;
            txtPartFriction.IsEnabled = false;
            txtPartSpeed.IsEnabled = false;

            txtPartAcceleration.Text = "";
            txtPartFriction.Text = "";
            txtPartName.Text = "";
            txtPartPrice.Text = "";
            txtPartSpeed.Text = "";

            switch (((ComboBoxItem)cbPartTypes.SelectedItem).Name)
            {
                case "cbiEngine":
                    lbParts.ItemsSource = Variables.Engines;                    
                    txtPartAcceleration.IsEnabled = true;
                    txtPartSpeed.IsEnabled = true;
                    break;
                case "cbiExhaust":
                    lbParts.ItemsSource = Variables.Exhausts;                                   
                    txtPartAcceleration.IsEnabled = true;
                    txtPartSpeed.IsEnabled = true;
                    break;
                case "cbiCarburetor":
                    lbParts.ItemsSource = Variables.Carburetors;                    
                    txtPartAcceleration.IsEnabled = true;
                    txtPartSpeed.IsEnabled = true;
                    break;
                case "cbiTires":
                    lbParts.ItemsSource = Variables.Tires;                    
                    txtPartFriction.IsEnabled = true;
                    break;
                case "cbiTurbo":
                    lbParts.ItemsSource = Variables.Turbos;
                    txtPartAcceleration.IsEnabled = true;
                    txtPartSpeed.IsEnabled = true;
                    break;
                case "cbiCooler":
                    lbParts.ItemsSource = Variables.Coolers;
                    txtPartAcceleration.IsEnabled = true;
                    txtPartSpeed.IsEnabled = true;
                    break;
                case "cbiGearbox":
                    lbParts.ItemsSource = Variables.Gearboxes;
                    txtPartAcceleration.IsEnabled = true;
                    txtPartFriction.IsEnabled = true;
                    txtPartSpeed.IsEnabled = true;
                    break;
                default:
                    break;
            }            
            e.Handled = true;
        }

        private void btnDeletePart_Click(object sender, RoutedEventArgs e)
        {
            switch (((ComboBoxItem)cbPartTypes.SelectedItem).Name)
            {
                case "cbiEngine":
                    Variables.Engines.Remove((Engine)lbParts.SelectedItem);                   
                    break;
                case "cbiExhaust":
                    Variables.Exhausts.Remove((Exhaust)lbParts.SelectedItem);
                    break;
                case "cbiCarburetor":
                    Variables.Carburetors.Remove((Carburetor)lbParts.SelectedItem);
                    break;
                case "cbiTires":
                    Variables.Tires.Remove((Tire)lbParts.SelectedItem);
                    break;
                case "cbiTurbo":
                    Variables.Turbos.Remove((Turbo)lbParts.SelectedItem);
                    break;
                case "cbiCooler":
                    Variables.Coolers.Remove((Cooler)lbParts.SelectedItem);
                    break;
                case "cbiGearbox":
                    Variables.Gearboxes.Remove((Gearbox)lbParts.SelectedItem);
                    break;
            }

            try
            {
                Variables.SaveComponents();
            }
            catch (Exception ex)
            {
                txtStatusBar.Text = ex.Message;
            }
        }

        private void lbParts_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (lbParts.SelectedItem != null)
            {
                Part part = (Part)lbParts.SelectedItem;

                txtPartName.Text = part.Name;

                if (txtPartAcceleration.IsEnabled)
                {
                    txtPartAcceleration.Text = part.AccelerationUpgrade.ToString();
                }
                if (txtPartSpeed.IsEnabled)
                {
                    txtPartSpeed.Text = part.SpeedUpgrade.ToString();
                }
                if (txtPartFriction.IsEnabled)
                {
                    txtPartFriction.Text = part.FrictionUpgrade.ToString();
                }

                txtPartPrice.Text = part.Price.ToString();
            }
        }

        private void btnSavePart_Click(object sender, RoutedEventArgs e)
        {
            if (lbParts.SelectedItem != null)
            {
                try
                {
                    checkPartFields();

                    ((Part)lbParts.SelectedItem).Name = txtPartName.Text;

                    if (txtPartSpeed.IsEnabled)
                    {
                        ((Part)lbParts.SelectedItem).SpeedUpgrade = Int32.Parse(txtPartSpeed.Text);
                    }

                    if (txtPartAcceleration.IsEnabled)
                    {
                        ((Part)lbParts.SelectedItem).AccelerationUpgrade = Int32.Parse(txtPartAcceleration.Text);
                    }

                    if (txtPartFriction.IsEnabled)
                    {
                        ((Part)lbParts.SelectedItem).FrictionUpgrade = Int32.Parse(txtPartFriction.Text);
                    }

                    ((Part)lbParts.SelectedItem).Price = Int32.Parse(txtPartPrice.Text);

                    lbParts.Items.Refresh();

                    Variables.SaveComponents();
                }
                catch (Exception ex)
                {
                    txtStatusBar.Text = ex.Message;
                }

            }
        }
        #endregion

        #endregion

        #region CAR

        //Tarkastaa auton lisäyksen eri kentät
        //Heittää exceptionin jos jotain puuttuu
        private void checkCarFields()
        {
            String name = txtCarName.Text;
            if (name == null || name.Length == 0)
            {
                throw new Exception("Nimi ei voi olla tyhjä!");
            }

            String icon = txtCarIcon.Text;
            if (icon == null || icon.Length == 0)
            {
                throw new Exception("Kuvapolku ei voi olla tyhjä!");
            }

            int price = 0;
            try
            {
                price = Int32.Parse(txtCarPrice.Text);
                if (price < 1)
                {
                    throw new Exception("Hinta ei voi olla negatiivinen");
                }
            }
            catch (Exception)
            {
                throw new Exception("Virheellinen hinnan arvo");
            }

            Engine engine = (Engine)cbEngines.SelectedItem;
            if (engine == null)
            {
                throw new Exception("Moottori täytyy olla valittuna!");
            }

            Exhaust exhaust = (Exhaust)cbExhausts.SelectedItem;
            if (exhaust == null)
            {
                throw new Exception("Pakoputkisto täytyy olla valittuna!");
            }

            Carburetor carburetor = (Carburetor)cbCarburetors.SelectedItem;
            if (carburetor == null)
            {
                throw new Exception("Kaasutin täytyy olla valittuna!");
            }

            Tire tires = (Tire)cbTires.SelectedItem;
            if (tires == null)
            {
                throw new Exception("Renkaat täytyy olla valittuna!");
            }

            Turbo turbo = (Turbo)cbTurbos.SelectedItem;

            Cooler cooler = (Cooler)cbCoolers.SelectedItem;

            if ((cooler != null && turbo == null) || (cooler.Name != "Ei intercooleria" && turbo.Name == "Ei turboa"))
            {
                throw new Exception("Turbo täytyy olla valittuna, jos haluat intercoolerin!");
            }

            Gearbox gearbox = (Gearbox)cbGearboxes.SelectedItem;
            if (gearbox == null)
            {
                throw new Exception("Vaihdelaatikko täytyy olla valittuna!");
            }
        }

        //Lisää auton kokoelmaan
        private void addCar()
        {
            try
            {
                checkCarFields();

                String name = txtCarName.Text;
                String icon = txtCarIcon.Text;
                int price = Int32.Parse(txtCarPrice.Text);
                Engine engine = (Engine)cbEngines.SelectedItem;
                Exhaust exhaust = (Exhaust)cbExhausts.SelectedItem;
                Carburetor carburetor = (Carburetor)cbCarburetors.SelectedItem;
                Tire tires = (Tire)cbTires.SelectedItem;
                Turbo turbo = (Turbo)cbTurbos.SelectedItem;
                Cooler cooler = (Cooler)cbCoolers.SelectedItem;
                Gearbox gearbox = (Gearbox)cbGearboxes.SelectedItem;

                if (turbo == null)
                {
                    turbo = new Turbo();
                }
                if (cooler == null)
                {
                    cooler = new Cooler();
                }

                Variables.Cars.Add(new Car(name, icon, price, engine, exhaust, carburetor, tires, turbo, cooler, gearbox));

                Variables.SaveComponents();

                updateTreeView();
            }
            catch (Exception ex)
            {
                txtStatusBar.Text = ex.Message;
            }        
        }

        //Hakee vastaavan osan combobokseista
        private void chooseMatchingPart(ComboBox cb, Part part)
        {
            Part temp;
            for (int i = 0; i < cb.Items.Count; i++)
            {
                temp = (Part)cb.Items[i];
                if (temp == part)
                {
                    cb.SelectedIndex = i;
                    return;
                }
            }
        }

        //Päivittää auto-treeviewin
        private void updateTreeView()
        {
            twCars.Items.Clear();
            foreach (Car item in Variables.Cars)
            {
                TreeViewItem twItem = new TreeViewItem();
                twItem.Header = item;

                twItem.Items.Add(item.Engine);
                twItem.Items.Add(item.Exhaust);
                twItem.Items.Add(item.Carburetor);
                twItem.Items.Add(item.Tires);
                twItem.Items.Add(item.Turbo);
                twItem.Items.Add(item.Cooler);
                twItem.Items.Add(item.Gearbox);                

                twCars.Items.Add(twItem);
            }
        }

        #region ACTIONHANDLING
        private void btnAddCar_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                addCar();
            }
            catch (Exception ex)
            {
                txtStatusBar.Text = ex.Message;
            }
        }

        private void btnDeleteCar_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Variables.Cars.Remove((Car)((TreeViewItem)twCars.SelectedItem).Header);
                Variables.SaveComponents();
            }
            catch (Exception ex)
            {
                txtStatusBar.Text = ex.Message;
            }
            
            updateTreeView();
        }

        private void twCars_Loaded(object sender, RoutedEventArgs e)
        {
            updateTreeView();
        }

        private void twCars_SelectedItemChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            if (twCars.SelectedItem != null && twCars.SelectedItem is TreeViewItem && ((TreeViewItem)twCars.SelectedItem).Header is Car)
            {
                Car car = (Car)((TreeViewItem)twCars.SelectedItem).Header;

                txtCarName.Text = car.Name;
                txtCarIcon.Text = car.Icon;
                txtCarPrice.Text = car.Price.ToString();

                chooseMatchingPart(cbEngines, car.Engine);
                chooseMatchingPart(cbExhausts, car.Exhaust);
                chooseMatchingPart(cbCarburetors, car.Carburetor);
                chooseMatchingPart(cbTires, car.Tires);
                chooseMatchingPart(cbTurbos, car.Turbo);
                chooseMatchingPart(cbCoolers, car.Cooler);
                chooseMatchingPart(cbGearboxes, car.Gearbox);                
            }
        }

        private void btnSaveCar_Click(object sender, RoutedEventArgs e)
        {
            if (twCars.SelectedItem != null && twCars.SelectedItem is TreeViewItem && ((TreeViewItem)twCars.SelectedItem).Header is Car)
            {
                try
                {
                    checkCarFields();

                    Turbo turbo = (Turbo)cbTurbos.SelectedItem;
                    Cooler cooler = (Cooler)cbCoolers.SelectedItem;

                    ((Car)((TreeViewItem)twCars.SelectedItem).Header).Name = txtCarName.Text;
                    ((Car)((TreeViewItem)twCars.SelectedItem).Header).Icon = txtCarIcon.Text;
                    ((Car)((TreeViewItem)twCars.SelectedItem).Header).Price = Int32.Parse(txtCarPrice.Text);
                    ((Car)((TreeViewItem)twCars.SelectedItem).Header).Engine = (Engine)cbEngines.SelectedItem;
                    ((Car)((TreeViewItem)twCars.SelectedItem).Header).Exhaust = (Exhaust)cbExhausts.SelectedItem;
                    ((Car)((TreeViewItem)twCars.SelectedItem).Header).Carburetor = (Carburetor)cbCarburetors.SelectedItem;
                    ((Car)((TreeViewItem)twCars.SelectedItem).Header).Tires = (Tire)cbTires.SelectedItem;
                    ((Car)((TreeViewItem)twCars.SelectedItem).Header).Turbo = (Turbo)cbTurbos.SelectedItem;
                    ((Car)((TreeViewItem)twCars.SelectedItem).Header).Cooler = (Cooler)cbCoolers.SelectedItem;
                    ((Car)((TreeViewItem)twCars.SelectedItem).Header).Gearbox = (Gearbox)cbGearboxes.SelectedItem;

                    updateTreeView();

                    Variables.SaveComponents();
                }
                catch (Exception ex)
                {
                    txtStatusBar.Text = ex.Message;
                }
            }
        }
        #endregion

        #endregion
    }
}
