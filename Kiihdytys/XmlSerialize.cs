﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Kiihdytys
{
    //Eri objektien serialisointia varten
    public static class XmlSerialize<T>
    {
        //Serialisointi
        public static void Serialize(String fileName, T obj)
        {
            String directory = Directory.GetParent(fileName).ToString();
            if (!Directory.Exists(directory))
            {
                try
                {
                    Directory.CreateDirectory(directory);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }

            XmlSerializer xs = new XmlSerializer(typeof(T));
            TextWriter tw = new StreamWriter(fileName);

            try
            {
                xs.Serialize(tw, obj);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                tw.Close();
            }
        }

        //Deserialisointi
        public static T Deserialize(String fileName)
        {
            XmlSerializer xs = new XmlSerializer(typeof(T));
           
            try
            {
                TextReader tr = new StreamReader(fileName);
                T obj = (T)xs.Deserialize(tr);
                tr.Close();
                return obj;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
