﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using System.Windows;

namespace Kiihdytys
{
    [Serializable]
    public class Game : INotifyPropertyChanged
    {
        #region PROPERTY

        private int day;
        private int totalDays;
        private int difficulty;
        private Player currentPlayer;
        private String weekday;
        private String debtMsg;       

        //Pelin vaikeusaste, vaikuttaa aloitusrahaan
        [XmlElement("Difficulty")]
        public int Difficulty
        {
            get
            {
                return difficulty;
            }
            set
            {
                if (value > -1 && value < 4) difficulty = value;
            }
        }

        //Tämänhetkinen päivä
        [XmlElement("Day")]
        public int Day
        {
            get
            {
                return day;
            }
            set
            {
                if (day == -1 && value > -1) day = value;
            }
        }

        //Pelattujen päivien määrä
        [XmlElement("TotalDays")]
        public int TotalDays
        {
            get
            {
                return totalDays;
            }
            set
            {
                if (totalDays == -1) totalDays = value;
            }
        }

        //Nykyinen pelaaja
        [XmlElement("CurrentPlayer")]
        public Player CurrentPlayer
        {
            get
            {
                return currentPlayer;
            }

            set
            {
                if (currentPlayer == null) currentPlayer = value;
            }
        }

        //Tämänhetkinen viikonpäivä
        [XmlElement("Weekday")]
        public String Weekday
        {
            get { return weekday; }
            set
            {
                weekday = value;
                Notify("Weekday");
            }
        }

        #endregion

        #region CONSTRUCTORS
        public Game()
        {
            day = -1;
            totalDays = -1;
        }

        //Peli-olion konstruktori, joka säätää aloitusrahan vaikeusasteen perusteella
        //Lataa pelille vaaditut muuttujat (Variables.LoadComponents())

        public Game(String playerName, int difficulty)
        {
            Variables.LoadComponents();
            
            int initialMoney;

            switch (difficulty)
            {
                case 1:
                    initialMoney = 10000;
                    break;
                case 2:
                    initialMoney = 7500;
                    break;
                case 3:
                    initialMoney = 5000;
                    break;
                default:
                    throw new Exception("Väärä vaikeusaste!");
            }

            currentPlayer = new Player(playerName, initialMoney);
            this.difficulty = difficulty;
            day = 0;
            totalDays = 0;
        }
        #endregion

        #region METHODS
        
        //Lainaa rahaa -metodi
        public void LoanMoney(int debt)
        {
            currentPlayer.Debt += (int)(debt*1.1);
            currentPlayer.Money += debt;
        }

        //Vaihtaa päivää, tarkistaa lainan
        public void NextDay()
        {
            day++;
            totalDays++;

            switch (day)
            {
                case 1:
                    Weekday = "Maanantai";
                    break;
                case 2:                    
                    Weekday = "Tiistai";
                    break;
                case 3:                    
                    Weekday = "Keskiviikko";
                    break;
                case 4:                    
                    Weekday = "Torstai";
                    break;
                case 5:                    
                    Weekday = "Perjantai";
                    break;
                case 6:                    
                    Weekday = "Lauantai";
                    break;
                case 7:
                    day = 0;
                    Weekday = "Sunnuntai";
                    currentPlayer.Money -= currentPlayer.Debt;
                    currentPlayer.Debt = 0;
                    break;
                default:
                    day = 7;
                    Weekday = "Maanantai";
                    break;
            }            
        }

        //Tallentaa pelin
        public void SaveGame()
        {
            XmlSerialize<Game>.Serialize("saves/" + currentPlayer.Name + ".sav", this);
        }

        #endregion

        #region OVERLOADS

        //Ylikuormitukset
        public override String ToString()
        {
            return currentPlayer.Name + ", Päivä " + totalDays + ", " + currentPlayer.Money + "€";
        }

        #endregion

        //Bindauksen päivittämistä varten
        #region INotifyPropertyChanged Members
        public event PropertyChangedEventHandler PropertyChanged;
        void Notify(String propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        #endregion

    }
}
