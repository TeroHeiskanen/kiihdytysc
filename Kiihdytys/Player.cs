﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Kiihdytys
{
    [Serializable]
    public class Player : INotifyPropertyChanged
    {
        #region PROPERTIES

        private String name;
        private int money;
        private List<Car> cars;
        private int index;
        private const int MAX_CARS = 4;
        private int debt;

        [XmlElement("Name")]
        public String Name { 
            get { return name; }
            set
            {
                if (value.Length > 0)
                {
                    name = value;
                    Notify("Name");
                }                    
            } 
        }

        [XmlElement("Money")]
        public int Money
        {
            get { return money; }
            set
            {
                money = value;
                Notify("Money");
            }
        }

        [XmlElement("Debt")]
        public int Debt
        {
            get { return debt; }
            set
            {
                if (value > -1)
                {
                    debt = value;
                    Notify("Debt");
                }
            }
        }

        //Palauttaa valitun auton indeksin
        [XmlElement("CarIndex")]
        public int CarIndex
        {
            get
            {
                return index;
            }
            set
            {
                if ((value > -1 && value < MAX_CARS)) index = value;
            }
        }

        //Palauttaa valitun auton
        public Car CurrentCar
        {
            get 
            {
                if (cars != null && index > -1 && index < cars.Count)
                {
                    return cars[index];
                }
                else
                {
                    return null;
                }
            }
        }

        //Kertoo onko tilaa autoille
        public bool RoomForCars
        {
            get
            {
                if (cars.Count >= 4)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
        }

        [XmlElement("Cars")]
        public List<Car> Cars
        {
            get
            {
                return cars;
            }
            set
            {
                if (cars.Count == 0) cars = value;
            }
        }
        #endregion

        #region CONSTRUCTORS
        public Player()
        {
            money = -1;
            index = -1;
            name = "";
            cars = new List<Car>(MAX_CARS);
            debt = 0;
        }

        public Player(String name, int money)
        {
            this.name = name;
            this.money = money;
            index = -1;
            cars = new List<Car>(MAX_CARS);
            debt = 0;
        }
        #endregion

        #region METHODS
        
        //Lisää auton, jos on tilaa
        public void AddCar(Car car)
        {
            if (cars.Count < MAX_CARS)
            {
                cars.Add(car);
                index++;
            }
            else
            {
                throw new Exception("Voit omistaa maksimissaan " + MAX_CARS + " autoa kerrallaan!");
            }
        }

        //Poistaa auton
        public void DeleteCar(Car car)
        {
            if (cars.Count > 0)
            {
                cars.Remove(car);
                index = 0;
                SortCars();
            }
            else
            {
                throw new Exception("Sinulla ei ole autoja!");
            }                       
        }

        //Järjestää autolistan niin, ettei välissä ole nulleja
        private void SortCars()
        {
            for (int i = 1; i < MAX_CARS; i++)
            {
                if (cars[i] != null && cars[i - 1] == null)
                {
                    cars[i - 1] = cars[i];
                    cars[i] = null;
                }
            }
            if (cars[0] == null)
            {
                index = -1;
            }
        }       

        #endregion

        //Bindauksen päivittämistä varten
        #region INotifyPropertyChanged Members
        public event PropertyChangedEventHandler PropertyChanged;
        void Notify(String propertyName)
        {
          if (PropertyChanged != null)
          {
            PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
          }
        }
        #endregion
    }
}
