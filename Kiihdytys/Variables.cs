﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kiihdytys
{
    //Sisältää pelille kaikki tärkeät muuttujat
    public static class Variables
    {
        #region PROPERTIES

        //Tiedostojen sijainnit
        public static String CAR_DIR = "data/cars.xml";
        public static String SAVE_DIR = "saves/";
        public static String PART_DIR = "data/parts";
        public static String ENGINE_DIR = PART_DIR + "/engines.xml";
        public static String EXHAUST_DIR = PART_DIR + "/exhausts.xml";
        public static String CARBURETOR_DIR = PART_DIR + "/carburetors.xml";
        public static String TIRE_DIR = PART_DIR + "/tires.xml";
        public static String TURBO_DIR = PART_DIR + "/turbos.xml";
        public static String COOLER_DIR = PART_DIR + "/coolers.xml";
        public static String GEARBOX_DIR = PART_DIR + "/gearboxes.xml";

        //Autojen ja osien kokoelmat
        public static ObservableCollection<Car> Cars = new ObservableCollection<Car>();
        public static ObservableCollection<Engine> Engines = new ObservableCollection<Engine>();
        public static ObservableCollection<Exhaust> Exhausts = new ObservableCollection<Exhaust>();
        public static ObservableCollection<Carburetor> Carburetors = new ObservableCollection<Carburetor>();
        public static ObservableCollection<Tire> Tires = new ObservableCollection<Tire>();
        public static ObservableCollection<Turbo> Turbos = new ObservableCollection<Turbo>();
        public static ObservableCollection<Cooler> Coolers = new ObservableCollection<Cooler>();
        public static ObservableCollection<Gearbox> Gearboxes = new ObservableCollection<Gearbox>();

        #endregion

        #region METHODS

        //Kokoelmien lataus
        public static void LoadComponents()
        {
            try
            {
                Cars = XmlSerialize<ObservableCollection<Car>>.Deserialize(CAR_DIR);
            }
            catch (Exception)
            {
                throw new Exception(CAR_DIR + " tiedostoa ei löydy!");
            }

            try
            {
                Engines = XmlSerialize<ObservableCollection<Engine>>.Deserialize(ENGINE_DIR);
            }
            catch (Exception)
            {
                throw new Exception(ENGINE_DIR + " tiedostoa ei löydy!");             
            }

            try
            {
                Exhausts = XmlSerialize<ObservableCollection<Exhaust>>.Deserialize(EXHAUST_DIR);
            }
            catch (Exception)
            {
                throw new Exception(EXHAUST_DIR + " tiedostoa ei löydy!");
            }

            try
            {
                Carburetors = XmlSerialize<ObservableCollection<Carburetor>>.Deserialize(CARBURETOR_DIR);
            }
            catch (Exception)
            {
                throw new Exception(CARBURETOR_DIR + " tiedostoa ei löydy!");
            }

            try
            {
                Tires = XmlSerialize<ObservableCollection<Tire>>.Deserialize(TIRE_DIR);
            }
            catch (Exception)
            {
                throw new Exception(TIRE_DIR + " tiedostoa ei löydy!");
            }

            try
            {
                Turbos = XmlSerialize<ObservableCollection<Turbo>>.Deserialize(TURBO_DIR);
            }
            catch (Exception)
            {
                throw new Exception(TURBO_DIR + " tiedostoa ei löydy!");
            }

            try
            {
                Coolers = XmlSerialize<ObservableCollection<Cooler>>.Deserialize(COOLER_DIR);
            }
            catch (Exception)
            {
                throw new Exception(COOLER_DIR + " tiedostoa ei löydy!");
            }

            try
            {
                Gearboxes = XmlSerialize<ObservableCollection<Gearbox>>.Deserialize(GEARBOX_DIR);
            }
            catch (Exception)
            {
                throw new Exception(GEARBOX_DIR + " tiedostoa ei löydy!");
            }
        }

        //Kokoelmien tallennus
        public static void SaveComponents()
        {
            try
            {
                XmlSerialize<ObservableCollection<Car>>.Serialize(CAR_DIR, Cars);
            }
            catch (Exception)
            {
                throw new Exception("Ei voitu tallentaa tiedostoon " + CAR_DIR);
            }

            try
            {
                XmlSerialize<ObservableCollection<Engine>>.Serialize(ENGINE_DIR, Engines);
            }
            catch (Exception)
            {
                throw new Exception("Ei voitu tallentaa tiedostoon " + ENGINE_DIR);
            }

            try
            {
                XmlSerialize<ObservableCollection<Exhaust>>.Serialize(EXHAUST_DIR, Exhausts);
            }
            catch (Exception)
            {
                throw new Exception("Ei voitu tallentaa tiedostoon " + EXHAUST_DIR);
            }

            try
            {
                XmlSerialize<ObservableCollection<Carburetor>>.Serialize(CARBURETOR_DIR, Carburetors);
            }
            catch (Exception)
            {
                throw new Exception("Ei voitu tallentaa tiedostoon " + CARBURETOR_DIR);
            }

            try
            {
                XmlSerialize<ObservableCollection<Tire>>.Serialize(TIRE_DIR, Tires);
            }
            catch (Exception)
            {
                throw new Exception("Ei voitu tallentaa tiedostoon " + TIRE_DIR);
            }

            try
            {
                XmlSerialize<ObservableCollection<Turbo>>.Serialize(TURBO_DIR, Turbos);
            }
            catch (Exception)
            {
                throw new Exception("Ei voitu tallentaa tiedostoon " + TURBO_DIR);
            }

            try
            {
                XmlSerialize<ObservableCollection<Cooler>>.Serialize(COOLER_DIR, Coolers);
            }
            catch (Exception)
            {
                throw new Exception("Ei voitu tallentaa tiedostoon " + COOLER_DIR);
            }

            try
            {
                XmlSerialize<ObservableCollection<Gearbox>>.Serialize(GEARBOX_DIR, Gearboxes);
            }
            catch (Exception)
            {
                throw new Exception("Ei voitu tallentaa tiedostoon " + GEARBOX_DIR);
            }
        }

        #endregion
    }
}
